module.exports = {
  appId: 'fansfront',
  name: 'fansfront',
  copyText: 'Copyright©2018 fans168',
  sysTitle: 'fansboot 管理平台',
  clientId: 'fhh',
  clientSecret: 'fhh',
  scope: 'fhh',
  grantType: 'password',
  authPre: 'Bearer ',
  logo: '',
  iconFontUrl: '',
  whiteList: ['/login', '/reset'],
  apiPrefix: '/fansb/admin/v1',
  devHost: 'http://127.0.0.1:8080',
  pubHost: 'http://127.0.0.1:8080',
  captchaEnable: true,
  /**
   * @description 是否使用国际化，默认为false
   *              如果不使用，则需要在路由中给需要在菜单中展示的路由设置meta: {title: 'xxx'}
   *              用来在菜单中显示文字
   */
  useI18n: false,
  api: {
    userLogin: '/auth/login',
    userLogout: '/auth/logout',
    captcha: '/auth/captcha?',
    isLogined: '/auth/isLogined',
    dict: '/sys/sysDict/getDictListByCode?code='
  }
}
