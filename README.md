# fansbfront

#### 项目介绍
fansbfront 是一套基于nodejs vue iview后台管理系统前端快速开发模板化解决方案，几句代码即可快速开发属于你的页面。

## 技术栈
  vue2 + vuex + vue-router + webpack + ES6 + axios + iview

#### 后端代码地址
  https://gitee.com/fhh/fans-boot

#### springcloud版本代码地址
前端：https://gitee.com/fhh/fans-cloud-front
后端：https://gitee.com/fhh/fans-cloud-alibaba


#### 安装教程

#### 安装依赖
npm install

#### 运行 localhost:8000
npm run dev
登录用户名/密码：admin/123456

#### 打包
npm run build

#### 引用关系

![输入图片说明](https://images.gitee.com/uploads/images/2019/0910/105203_5b4f4e61_79358.png "主要引用关系.png")

#### 软件架构
![输入图片说明](https://images.gitee.com/uploads/images/2019/0910/110459_705882d2_79358.png "软件架构.png")

#### 使用说明
安装依赖（npm install）时可能会出现node-sass 安装失败的问题，解决方法如下：
安装淘宝镜像源后使用 cnpm 安装 node-sass 会默认从淘宝镜像源下载：cnpm install node-sass

#### 运行效果

![登录](https://images.gitee.com/uploads/images/2019/0910/112019_47145f00_79358.png "登录")
![首页](https://images.gitee.com/uploads/images/2019/0910/111321_b4880644_79358.png "首页")
![用户管理](https://images.gitee.com/uploads/images/2019/0910/111604_e3f1cf85_79358.png "用户管理")
![菜单配置](https://images.gitee.com/uploads/images/2019/0910/111628_69d0e0f3_79358.png "菜单配置")
![角色管理](https://images.gitee.com/uploads/images/2019/0910/112326_472f02ab_79358.png "角色管理")
![字典管理](https://images.gitee.com/uploads/images/2019/0910/112338_9de7207b_79358.png "字典管理")
![oauth2客户端管理](https://images.gitee.com/uploads/images/2019/0910/113052_1513106d_79358.png "oauth2客户端管理")
![oauth2客户端编辑](https://images.gitee.com/uploads/images/2019/0910/113117_69dbb0f0_79358.png "oauth2客户端编辑.png")
![认证用户](https://images.gitee.com/uploads/images/2019/0910/113143_e1a1e1b6_79358.png "认证用户.png")
![swagger](https://images.gitee.com/uploads/images/2019/0910/113221_c6873d3b_79358.png "swagger.png")
![定时任务管理](https://images.gitee.com/uploads/images/2019/0910/113936_e178e214_79358.png "定时任务管理.png")
![定时任务表达式编辑](https://images.gitee.com/uploads/images/2019/0910/113954_8b352b43_79358.png "定时任务表达式编辑.png")
![代码生成器](https://images.gitee.com/uploads/images/2019/0910/114011_41fb2b23_79358.png "代码生成器.png")
![开发组件](https://images.gitee.com/uploads/images/2019/0910/114024_9b6bbdb2_79358.png "开发组件.png")

#### 参考网址
可以参考iview-admin：https://lison16.github.io/iview-admin-doc/#/

